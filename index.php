<?php
header("Content-Type: text/html; charset=utf-8");

$url = "https://www.liga.net/tech/gadgets/rss.xml"; // Адрес до RSS-ленты
$rss = simplexml_load_file($url);

foreach ($rss->channel->item as $items) {

$array=(array) $items->enclosure;
$imgurl = $array['@attributes']['url'];
    echo <<<HTML
			<h1>{$items->title}</h1>
			<p>{$items->description}</p>
			<img src="{$imgurl}"/>
			<a href="{$items->link}">Подробнее</a>
HTML;

}
